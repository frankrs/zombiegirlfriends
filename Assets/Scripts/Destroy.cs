﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {


	public float destroyTime = 1f;


	IEnumerator Start () {
		yield return new WaitForSeconds (destroyTime);
		Destroy (gameObject);
	}
	

}
