﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour {

	public float rotSpeed;

	public float fallSpeed;

	public GameObject burst;

	public delegate void RemoveHeart (GameObject heart);
	public static event RemoveHeart OnRemoveHeart;

	public delegate void ClickedHeart ();
	public static event ClickedHeart OnClickedHeart;

	void Start(){
		rotSpeed = Random.Range (rotSpeed * .5f, rotSpeed * 2f);
		fallSpeed = Random.Range (fallSpeed * .5f, fallSpeed * 2f);
	}

	void Update () {
		transform.Rotate (new Vector3(0f, rotSpeed * Time.deltaTime, 0f), Space.World);
		transform.Translate (-Vector3.up * fallSpeed * Time.deltaTime, Space.World);

		if (transform.position.y < -2f) {
			OnRemoveHeart (gameObject);
		}
	}
		
	public void Clicked(){
		OnClickedHeart ();
		GameObject burstPart = Instantiate (burst, transform.position, Quaternion.identity);
		OnRemoveHeart (gameObject);
	}

}
