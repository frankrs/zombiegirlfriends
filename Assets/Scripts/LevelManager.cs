﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LevelManager : MonoBehaviour {

	public Stats statsDisplay;

	public HeartSpawner heartSpawner;

	public ZombieSpawner zombieSpawner;

	public AudioClip[] musics;

	public AudioClip pop;

	private AudioSource musicPlayer;

	private ColorCorrectionCurves[] colCurs;

	void OnEnable(){
		Heart.OnRemoveHeart += OnRemoveHeart;
		Heart.OnClickedHeart += OnClickedHeart;
		Zombie.OnZombieHit += OnZombieHit;
		Zombie.OnDieZombie += OnDieZombie;
	}

	void OnDisable(){
		Heart.OnRemoveHeart -= OnRemoveHeart;
		Heart.OnClickedHeart -= OnClickedHeart;
		Zombie.OnZombieHit -= OnZombieHit;
		Zombie.OnDieZombie-= OnDieZombie;
	}


	void Start(){

		StaticStuff.dead = false;

		colCurs = FindObjectsOfType<ColorCorrectionCurves>();
		
		zombieSpawner.spawnPoints = GameObject.Find ("ZombieSpawns").GetComponentsInChildren<Transform> ();

		GvrViewer gvrViewr = FindObjectOfType<GvrViewer> ();
		gvrViewr.VRModeEnabled = StaticStuff.vrOn;

		musicPlayer = GetComponent<AudioSource> ();

		StartLevel ();
	}

	void StartLevel(){
		InvokeRepeating ("SpawnHeart", 1/heartSpawner.heartSpawnRate, 1/heartSpawner.heartSpawnRate);
		InvokeRepeating ("SpawnZombie", 5f, zombieSpawner.zombieSpawnRate);
		DrawStats ();
		StartCoroutine (DrawLevelMeters ());

		if (StaticStuff.musicOn) {
			int clipIndex = StaticStuff.level;
			while (clipIndex > musics.Length) {
				clipIndex = clipIndex - musics.Length;
			}
			musicPlayer.clip = musics [clipIndex - 1];
			musicPlayer.Play ();
		}

	}

	IEnumerator DrawLevelMeters(){
		foreach (Text t in statsDisplay.levelMeters) {
			t.text = "Level " + StaticStuff.level.ToString ();
			t.enabled = true;
		}
		yield return new WaitForSeconds (3f);
		foreach (Text t in statsDisplay.levelMeters) {
			t.enabled = false;
		}
	}

	void OnDrawGizmos(){
		Gizmos.DrawWireCube (heartSpawner.heartSpawnBounds.center, heartSpawner.heartSpawnBounds.extents * 2);
	}


	void SpawnHeart(){
		Vector3 spawnPos = new Vector3 (Random.Range (-heartSpawner.heartSpawnBounds.extents.x, heartSpawner.heartSpawnBounds.extents.x), Random.Range (-heartSpawner.heartSpawnBounds.extents.y, heartSpawner.heartSpawnBounds.extents.y), 
			Random.Range (-heartSpawner.heartSpawnBounds.extents.z, heartSpawner.heartSpawnBounds.extents.z)) + heartSpawner.heartSpawnBounds.center; 
		GameObject newHeart = GameObject.Instantiate (heartSpawner.heartPrefab, spawnPos, Quaternion.identity);
		heartSpawner.hearts.Add (newHeart);
	}


	void SpawnZombie(){
		if (zombieSpawner.zombiesSpawned < StaticStuff.level) {
			Transform spawnPos = zombieSpawner.spawnPoints [Random.Range (0, zombieSpawner.spawnPoints.Length - 1)];
			GameObject newZombie = GameObject.Instantiate (zombieSpawner.zombiePrefab, spawnPos.position, Quaternion.identity);
			zombieSpawner.zombies.Add (newZombie);
			zombieSpawner.zombiesSpawned++;
		}
	}


	void OnClickedHeart(){
		musicPlayer.PlayOneShot (pop, 1f);
		if (StaticStuff.hearts < StaticStuff.maxedHearts) {
			StaticStuff.hearts++;
		}
		if (StaticStuff.hearts == StaticStuff.maxedHearts) {
			ActivateLoved (true);
		}
		DrawStats ();
	}

	void ActivateLoved(bool isLoved){
		if (isLoved) {
			foreach (ColorCorrectionCurves c in colCurs) {
				c.enabled = true;
				c.saturation = 5f;
			}
		}
		if (!isLoved) {
			foreach (ColorCorrectionCurves c in colCurs) {
				c.enabled = false;
				c.saturation = 1f;
			}
		}
	}

	void OnRemoveHeart(GameObject heart){
		heartSpawner.hearts.Remove (heart);
		Destroy (heart);
	}

	void OnZombieHit(){
		if (StaticStuff.dead) {
			return;
		}
		StaticStuff.hearts = StaticStuff.hearts - 2;
		DrawStats ();
		if (StaticStuff.hearts < 1) {
			Die ();
		} 
		else {
			ActivateLoved (false);
		}
	}

	void Die(){
		StaticStuff.dead = true;
		StartCoroutine (FadeOutDie ());
	}

	IEnumerator FadeOutDie(){
		foreach (ColorCorrectionCurves cCC in colCurs) {
			cCC.enabled = true;
		}
		float fadeTime = 3f;
		while (fadeTime > 0f) {
			foreach (ColorCorrectionCurves cCC in colCurs) {
				cCC.saturation = fadeTime / 3f;
			}
			fadeTime = fadeTime - Time.deltaTime;
			yield return null;
		}
		StaticStuff.level = 1;
		StaticStuff.hearts = 0;
		StaticStuff.maxedHearts = 10;
		zombieSpawner.zombiesKilled = 0;
		zombieSpawner.zombiesSpawned = 0;
		SceneManager.LoadScene(0);
	}

	void OnDieZombie(GameObject zombie){
		zombieSpawner.zombies.Remove (zombie);
		zombieSpawner.zombiesKilled++;
		Destroy (zombie);
		if (zombieSpawner.zombiesKilled == StaticStuff.level) {
			LevelUP ();
		}
	}

	void LevelUP(){
		StaticStuff.level++;
		StaticStuff.hearts = 0;
		StaticStuff.maxedHearts = 10 * StaticStuff.level;
		zombieSpawner.zombiesKilled = 0;
		zombieSpawner.zombiesSpawned = 0;
		ActivateLoved (false);
		DrawStats ();
		CancelInvoke ("SpawnHeart");
		CancelInvoke ("SpawnZombie");
		foreach (GameObject h in heartSpawner.hearts) {
			Destroy (h);
		}
		heartSpawner.hearts.Clear ();
		zombieSpawner.zombies.Clear ();
		StartLevel ();
	}

	void DrawStats(){
		statsDisplay.hearts = StaticStuff.hearts;
		statsDisplay.maxedHearts = StaticStuff.maxedHearts;
		statsDisplay.level = StaticStuff.level;
		foreach (Image h in statsDisplay.heartGUI) {
			h.color = Color.Lerp (Color.black, Color.white, (float)StaticStuff.hearts / (float)StaticStuff.maxedHearts);
		}
	}

}

[System.Serializable]
public class Stats{
	public int hearts = 0;
	public int maxedHearts = 10;
	public int level = 1;
	public Image[] heartGUI;
	public Text[] levelMeters;
}

[System.Serializable]
public class HeartSpawner{
	public GameObject heartPrefab;
	public Bounds heartSpawnBounds;
	public float heartSpawnRate;
	public List<GameObject> hearts;
}

[System.Serializable]
public class ZombieSpawner{
	public GameObject zombiePrefab;
	public Transform[] spawnPoints;
	public float zombieSpawnRate;
	public int zombiesSpawned = 0;
	public int zombiesKilled = 0;
	public List <GameObject> zombies;
}


public static class StaticStuff{
	public static int hearts = 0;
	public static int maxedHearts = 10;
	public static int level = 1;
	public static bool dead = false;
	public static bool vrOn = true;
	public static bool musicOn = true;
	public static bool isLoved = false;
}