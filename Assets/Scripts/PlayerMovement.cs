﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {

	public Transform lookTran;

	public LayerMask clickable;

	public NavMeshAgent navAgent;

	public Bounds playerBounds;


	void Update () {
		if (StaticStuff.dead) {
			return;
		}
		if (Input.GetMouseButtonDown(0)) {
			LookForClick ();
		}
	}


	void LookForClick(){
		RaycastHit hit;
		if (Physics.Raycast (lookTran.position, lookTran.forward, out hit, 500f, clickable)) {
			GameObject go = hit.collider.gameObject;
			go.SendMessageUpwards ("Clicked", SendMessageOptions.DontRequireReceiver);
			if (go.CompareTag ("Ground")) {
				NavMeshHit navHit;
				if (NavMesh.SamplePosition (hit.point, out navHit, 3f, NavMesh.AllAreas)) {
					if (playerBounds.Contains (navHit.position)) {
						navAgent.SetDestination (navHit.position);
					}
				}
			}
		}
	}


	void OnDrawGizmos(){
		Gizmos.DrawWireCube (playerBounds.center, playerBounds.extents * 2);
	}


}
