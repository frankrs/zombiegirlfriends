﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {

	public AudioClip[] musics;

	private AudioSource musicPlayer;

	public Toggle vrOnToggle;

	public Toggle musicOnToggle;


	void Update(){
		if (Input.GetKeyDown ("escape")) {
			Application.Quit ();
		}
	}


	void Start(){
		
		musicPlayer = GetComponent<AudioSource> ();

		if (StaticStuff.musicOn) {
			musicPlayer.clip = musics [Random.Range (0, musics.Length - 1)];
			musicPlayer.Play ();
		}

		if (StaticStuff.vrOn) {
			vrOnToggle.isOn = true;
		} else {
			vrOnToggle.isOn = false;
		}

		if (StaticStuff.musicOn) {
			musicOnToggle.isOn = true;
		} else {
			musicOnToggle.isOn = false;
		}
			
	}

	public void ToggleVR(){
		if (StaticStuff.vrOn) {
			StaticStuff.vrOn = false;
		} else {
			StaticStuff.vrOn = true;
		}
	}

	public void ToggleMusic(){
		if (StaticStuff.musicOn) {
			StaticStuff.musicOn = false;
			musicPlayer.Stop ();
		} else {
			StaticStuff.musicOn = true;
			musicPlayer.Play ();
		}
	}

	public void StartGame(){
		SceneManager.LoadScene (1);
	}

}
