﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour {

	GameObject player;

	NavMeshAgent navAgent;

	Animator anim;

	public delegate void ZombieHit();
	public static event ZombieHit OnZombieHit;

	public delegate void DieZombie(GameObject zombie);
	public static event DieZombie OnDieZombie;

	public enum ZombieState {chassing, attacking, running, hurt, dying};
	public ZombieState zombieState;

	public float attackRange = 1f;

	public float runFromTime = 5f;

	public AudioClip[] walkingClips;

	public AudioClip dieClip;

	public AudioClip attackClip;

	public AudioClip hurtClip;

	float runTimer;

	AudioSource audioS;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		navAgent = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		audioS = GetComponent <AudioSource> ();
		audioS.clip = walkingClips [Random.Range (0, walkingClips.Length - 1)];
		audioS.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		switch (zombieState) {
		case ZombieState.chassing:
			navAgent.destination = player.transform.position;
			if (Vector3.Distance (transform.position, player.transform.position) < attackRange) {
				SetAttackingState ();
			}
			if (!audioS.isPlaying) {
				audioS.clip = walkingClips [Random.Range (0, walkingClips.Length - 1)];
				audioS.Play ();
			}
			break;
		case ZombieState.attacking:
			if (Vector3.Distance (transform.position, player.transform.position) > attackRange + 1f) {
				SetChassingState ();
			}
			transform.LookAt (player.transform.position, Vector3.up);
			break;
		case ZombieState.hurt:

			break;
		case ZombieState.running:
			runTimer = runTimer - Time.deltaTime;
			if (runTimer < 0) {
				SetChassingState ();
			}
			if (!audioS.isPlaying) {
				audioS.clip = walkingClips [Random.Range (0, walkingClips.Length - 1)];
				audioS.Play ();
			}
			break;
		}
	}


	void SetChassingState(){
		zombieState = ZombieState.chassing;
		anim.SetTrigger ("Chassing");
		navAgent.speed = .5f;
	}


	void SetAttackingState(){
		zombieState = ZombieState.attacking;
		navAgent.Stop ();
		navAgent.ResetPath ();
		anim.SetTrigger ("Attacking");
	}

	void SetHurtState(){
		zombieState = ZombieState.hurt;
		navAgent.Stop ();
		navAgent.ResetPath ();
		anim.SetTrigger ("Hurt");
		audioS.clip = hurtClip;
		audioS.Play ();
	}

	void SetRunningState(Transform runToSpawn){
		//Debug.Log (runToSpawn);
		zombieState = ZombieState.running;
		runTimer = runFromTime;
		navAgent.SetDestination (runToSpawn.position);
		navAgent.speed = 5f;
		anim.SetTrigger ("Running");
	}


	void SetDyingState(){
		zombieState = ZombieState.dying;
		navAgent.Stop ();
		navAgent.ResetPath ();
		anim.SetTrigger ("Dying");
		audioS.clip = dieClip;
		audioS.Play();
	}

	public void Attack(){
		if (Vector3.Distance (player.transform.position, transform.position) < attackRange) {
			OnZombieHit ();
			audioS.PlayOneShot (attackClip);
		}
	}

	public void Recover(){
		Transform[] spawns = FindObjectOfType<LevelManager> ().zombieSpawner.spawnPoints;
		float closest = 180f;
		int spawnIndex = 0;
		Transform cam = Camera.main.transform;
		for (int i = 0; i<spawns.Length-1; i++) {
			Vector3 targetDir = spawns[i].position - player.transform.position;
			float angle = Vector3.Angle(targetDir, cam.forward);
//			Debug.Log (angle);
//			Debug.Log (spawns [i]);
			if (angle < closest) {
				closest = angle;
				spawnIndex = i;
			}
		}
		SetRunningState (spawns [spawnIndex]);
	}

	public void Clicked(){
		if (zombieState != ZombieState.hurt && zombieState != ZombieState.dying) {
			if (StaticStuff.hearts == StaticStuff.maxedHearts) {
				SetDyingState ();
			} 
			else {
				SetHurtState ();
			}
		}
	}


	public void DeathAnimPlayed(){
		OnDieZombie (gameObject);
	}

}
